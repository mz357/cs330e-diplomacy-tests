#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, Army, City

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, ("Hold", None))

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, ("Move", "Madrid"))

    def test_read_3(self):
        s = "E Austin Support A\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "E")
        self.assertEqual(j, "Austin")
        self.assertEqual(k, ("Support", "A"))
        
    def test_read_4(self):
        s = "D Austin Move London\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "D")
        self.assertEqual(j, "Austin")
        self.assertEqual(k, ("Move", "London"))

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, [('A', '[dead]'),
                             ('B', '[dead]'),
                             ('C', '[dead]'),
                             ('D', 'Paris'),
                             ('E', 'Austin')])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [('A', '[dead]'), ('B', 'Madrid'), ('C', '[dead]'), ('D', 'Paris')])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [('A', '[dead]'), ('B', '[dead]')])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
         
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Paris\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
